#lang racket
(require xenomorph
         vec)

(define head int32) ;#"BMF\3"

(define info
  (x:dict
    'bsize          uint32
    'font-size      int16
    'bit-field      (x:bitfield uint8 '(smooth unicode italic bold fixed-height #false #false #false))
    'charset        uint8
    'stretch-h      uint16
    'aa             uint8
    'padding-up     uint8
    'padding-right  uint8
    'padding-down   uint8
    'padding-left   uint8
    'spacing-horiz  uint8
    'spacing-vert   uint8
    'outline        uint8
    'font-name      (x:string)))

(define common
  (x:dict
    'bsize        uint32
    'line-height  uint16
    'base         uint16
    'scale-w      uint16
    'scale-h      uint16
    'pages        uint16
    'bit-field    (x:bitfield uint8 '(packed))
    'alpha        uint8
    'red          uint8
    'green        uint8
    'blue         uint8))

(define (decode-chars data)
  (let* ([bsize (integer-bytes->integer (subbytes data 0 4) #f)]
         [d (subbytes data 4 (+ 4 bsize))])
    (hash 'chars
          (decode
            (x:list
              (x:dict
                'id       uint32
                'x        uint16
                'y        uint16
                'width    uint16
                'height   uint16
                'x-offset int16
                'y-offset int16
                'xadvance int16
                'page     uint8
                'channel  uint8)
              (/ bsize 20))
            d)
          'bsize bsize)))

(define (decode-kerning data)
  (let* ([bsize (integer-bytes->integer (subbytes data 0 4) #f)]
         [d (subbytes data 4 (+ 4 bsize))])
    (hash 'kerning
          (decode
            (x:list
              (x:dict
                'first  uint32
                'second uint32
                'amount int16))
            d)
          'bsize bsize)))

(define (decode-pages data)
  (let* ([bsize (integer-bytes->integer (subbytes data 0 4) #f)]
         [d (subbytes data 4 (+ 4 bsize))])
    (hash 'strings (decode (x:list (x:string)) d)
          'bsize   bsize)))

(define (decode-block b)
  (case (bytes-ref b 0)
    [(1) (values 'info    (decode info     (subbytes b 1)))]
    [(2) (values 'common  (decode common   (subbytes b 1)))]
    [(3) (values 'pages   (decode-pages    (subbytes b 1)))]
    [(4) (values 'chars   (decode-chars    (subbytes b 1)))]
    [(5) (values 'kerning (decode-kerning  (subbytes b 1)))]))

(provide decode-file)
(define (decode-file f)
  (call-with-input-file f
    (λ (port)
      (let ([d (port->bytes port)])
        (if (= (integer-bytes->integer #"BMF\3" 4) (decode head d))
          (let loop ([decoded (hash)]
                     [encoded (subbytes d 4)])
            (if (= 0 (bytes-length encoded))
              decoded
              (let-values ([(type dec) (decode-block encoded)])
                (loop (hash-set decoded type dec)
                      (subbytes encoded (+ 5 (hash-ref dec 'bsize)))))))
          (raise-user-error (format "Tried to decode ~a as a BMF, but the header was wrong" f)))))))

(provide current-font)
(define current-font (make-parameter #f))

(provide with-font)
(define-syntax-rule (with-font f body ...)
  (parameterize ([current-font f])
    body ...))

(provide find-char)
(define (find-char c)
  ;(displayln c)
  (findf (λ (char) (= (char->integer c) (hash-ref char 'id)))
          (hash-ref* (current-font) 'chars 'chars)))

(provide char-advance)
(define (char-advance c)
  (let ([cd (find-char c)])
    (if cd
      (hash-ref cd 'xadvance)
      #f)))

(provide char-size)
(define (char-size c)
  (let ([cd (find-char c)])
    (if cd
      (vec2 (hash-ref cd 'width) (hash-ref cd 'height))
      #f)))

(provide char-page)
(define (char-page c)
  (let ([cd (find-char c)])
    (if cd
      (hash-ref cd 'page)
      #f)))

(provide advance)
(define (advance c)
  (or (char-advance c)
      (char-advance #\M)
      (char-advance #\m)
      0))

(provide advance-size)
(define (advance-size s)
  (if (non-empty-string? s)
    (for/fold ([r 0]
               [p #\nul]
               #:result r)
      ([c (in-string s)])
      (values (+ r (advance c) (kerning p c)) c))
    0))

(provide kerning)
(define (kerning a b)
  (if (and (hash-has-key? (current-font) 'kerning))
    (let ([kr (findf (λ (k) (and (= (char->integer a) (hash-ref k 'first))
                                 (= (char->integer b) (hash-ref k 'second))))
                     (hash-ref* (current-font) 'kerning 'kerning))])
      (if kr
        (hash-ref kr 'amount)
        0))
    0))

(provide valid-char?)
(define (valid-char? id)
  (and  (> id 0)
        (or (<= id #xD7FF)
            (>= id #xE000))
        (<= id #x10FFFF)))

(define (dump-valid-chars)
  (unless (current-font) (raise-user-error "No font."))
  (with-output-to-file
    (string-append (hash-ref* (current-font) 'info 'font-name) ".character-dump.txt")
    (thunk
      (display
        (string-join
          (for/list ([(char i) (in-indexed (hash-ref* (current-font) 'chars 'chars))])
            (let ([id (hash-ref char 'id)])
              (if (= id #xFFFFFFFF)
                (~a (~a i #:min-width 3 #:align 'right) " | unrepresentable character glyph : present")
                (if (valid-char? id)
                (~a (~a i #:min-width 3 #:align 'right) " | "
                    (~a id #:min-width 6 #:align 'right) " | "
                    (~a "#\\u" (~r id #:base 16) #:min-width 8) " | "
                    (integer->char id))
                (~a (~a i #:min-width 3 #:align 'right) " | ??? : " id)))))
          "\n")))
    #:exists 'truncate))
