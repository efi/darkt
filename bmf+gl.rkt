#lang racket
(require vec
         "bmf.rkt"
         "gl.rkt"
         "sprite.rkt"
         "hash-ref-asterisk.rkt")

(define text-fb (make-parameter #f))
(provide init-text!)
(define (init-text!)
  (in-gl (text-fb (gen-framebuffers))
         (report-gl-error)))

(define (text-to-pixel-size font s [w (first (get-client-size))])
  (for/fold ([width 0]
             [max-w 0]
             [height 1]
             [p #\nul]
             #:result (list max-w (* (hash-ref* font 'data 'common 'line-height) height)))
            ([c (in-string s)])
    (let ([a (+ (advance c) (kerning p c))])
      (if (>= (+ a width) w)
        (values 0 (max max-w width) (add1 height) c)
        (values (+ a width) (max max-w (+ a width)) height c)))))

(define text-map (hash))
(provide render-text)
(define (render-text font s [pos (vec2 0.0)] [w (first (get-client-size))])
  (when (non-empty-string? s)
    (with-font (hash-ref font 'data)
      (let ([n (~a s ":" (hash-ref font 'name) ":" w)]
            [size (text-to-pixel-size font s w)])
        (unless (and (> (first size)  0)
                     (> (second size) 0))
          (raise-user-error "Rendering text with size 0?"))
        (if (hash-has-key? text-map n)
          (render pos (hash-ref* text-map n 'texture) (list* 0 0 (hash-ref* text-map n 'size)) (vec2 1.0 1.0))
          (set! text-map
            (hash-set text-map n
                      (hash
                        'size size
                        'texture (apply prerender-text font s size)))))))))

(provide draw-text!)
(define (draw-text! font s [pos (vec2 0.0 (second (get-client-size)))] [w (first (get-client-size))])
  (when (non-empty-string? s)
    (with-font (hash-ref font 'data)
      (let ([size (text-to-pixel-size font s w)]
            [lh (hash-ref* font 'data 'common 'line-height)]
            [bl (hash-ref* font 'data 'common 'base)])
        (unless (and (> (first size)  0)
                     (> (second size) 0))
          (raise-user-error "Rendering text with size 0?"))
        (apply uniform "viewSize" 'f (map exact->inexact (get-client-size)))
        (for ([(i a) (in-hash (hash-ref font 'pages))])
          (active-texture i)
          (bind-texture (Texture-binding (Atlas-texture a)))
          (report-gl-error))
        (enable 'GL_BLEND)
        (blend-function 'GL_SRC_ALPHA 'GL_ONE_MINUS_SRC_ALPHA)
        (for/fold ([adv (vec2 0.0 0.0)]
                   [p #\nul]
                   #:result (void))
          ([c (in-list (string->list s))])
          (draw-char font c (vec+ (vec* (vec2 1.0 -1.0) pos) adv) (vec2 1.0))
          (let* ([xadv (+ (vec-x adv) (advance c) (kerning p c))]
                 [yadv (if (>= xadv w)
                         (+ (vec-y adv) lh)
                         (vec-y adv))])
            (values (vec2 (if (>= xadv w) 0.0 xadv) yadv) c)))))))

(define printf-buffer (make-parameter (vector)))
(provide printf-draw!)
(define (printf-draw! font)
  (for ([(line n) (in-indexed (in-vector (printf-buffer)))])
    (draw-text! font line
                (vec2 0.0 (- (second (get-client-size)) (* n (hash-ref* font 'data 'common 'line-height))))))
  (printf-buffer (vector)))
(provide printf!)
(define (printf! s . p)
  (printf-buffer (vector-append (printf-buffer) (vector (apply format s p)))))

(define (draw-char font c [pos (vec2 0.0)] [scale (vec2 1.0 -1.0)])
  (when (hash-has-key? (hash-ref font 'letters) (char->integer c))
    (let* ([cn (char->integer c)]
           [page (hash-ref* font 'letters cn)]
           [slice (hash-ref (Atlas-slices (hash-ref* font 'pages page)) cn)]
           [char-data (find-char c)]
           [cox (hash-ref char-data 'x-offset)]
           [coy (hash-ref char-data 'y-offset)]
           [chh (hash-ref char-data 'height)])
      (uniform "inTexture" 'i page)
      (apply uniform "viewSize" 'f (map exact->inexact (get-client-size)))
      (apply uniform "position" 'f (vec+ (vec2 (vec-x pos) (- (vec-y pos))) (vec2 cox (- (+ coy chh)))))
      (uniform "origin" 'f 0.0 0.0)
      (apply uniform "rect" 'f (Slice-rect slice))
      (apply uniform "scale" 'f scale)
      (uniform "rotation" 'f 0.0)
      (draw-elements 'GL_TRIANGLES 6 'GL_UNSIGNED_INT)
      (report-gl-error))))

(define (prerender-text font s w h)
  (let ([texture (first (gen-textures))]
        [lh (hash-ref* font 'data 'common 'line-height)]
        [bl (hash-ref* font 'data 'common 'base)])
    (bind-framebuffer (text-fb))
    (bind-texture texture)
    (tex-storage-2D w h)
    (tex-parameter 'GL_TEXTURE_MAG_FILTER #x2600)
    (tex-parameter 'GL_TEXTURE_MIN_FILTER #x2600)
    (bind-texture-to-framebuffer texture)
    (apply uniform "viewSize" 'f (map exact->inexact (get-client-size)))
    (for ([(i a) (in-hash (hash-ref font 'pages))])
      (active-texture i)
      (bind-texture (Texture-binding (Atlas-texture a)))
      (report-gl-error))
    (enable 'GL_BLEND)
    (blend-function 'GL_SRC_ALPHA 'GL_ONE_MINUS_SRC_ALPHA)
    (for/fold ([adv (vec2 0.0 0.0)]
               [p #\nul]
               #:result (void))
              ([c (in-list (string->list s))])
      (draw-char font c adv)
      (let* ([xadv (+ (vec-x adv) (advance c) (kerning p c))]
             [yadv (if (>= xadv w)
                     (+ (vec-y adv) lh)
                     (vec-y adv))])
        (values (vec2 (if (>= xadv w) 0.0 xadv) yadv) c)))
    (bind-framebuffer 0)
    (Texture texture w h)))

(provide load-font)
(define (load-font f)
  (unless (file-exists? f) (raise-user-error (format "Font file ~a not found" f)))
  (let* ([font (decode-file f)]
         [pages (for/hash ([(page i) (in-indexed (hash-ref* font 'pages 'strings))])
                  (values i (texture-from-file page)))]
         [page-atlas
           (for/hash ([(page texture) (in-hash pages)])
             (for*/fold ([ah (hash)]
                         #:result (values page (Atlas (~a f ":" page) texture ah)))
                        ([c  (in-list (filter
                                        (λ (c) (= page (hash-ref c 'page)))
                                        (hash-ref* font 'chars 'chars)))]
                         [id (in-value (hash-ref c 'id))])
               (hash-set ah id 
                         (Slice id
                                (map exact->inexact
                                     (vec2 (- (hash-ref c 'x-offset)) (- (hash-ref c 'height) (hash-ref c 'y-offset))))
                                (map exact->inexact
                                     (vec4 (hash-ref c 'x)
                                           (hash-ref c 'y)
                                           (+ (hash-ref c 'width) (hash-ref* font 'info 'outline))
                                           (+ (hash-ref c 'height) (hash-ref* font 'info 'outline))))
                                #f 0))))])
    (hash
      'name (hash-ref* font 'info 'font-name)
      'data font
      'pages page-atlas
      'letters (for/hash ([c (in-list (hash-ref* font 'chars 'chars))])
                 (values (hash-ref c 'id) (hash-ref c 'page))))))
