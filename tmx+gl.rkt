#lang racket
(require "sprite.rkt"
         "gl.rkt"
         "tmx.rkt"
         vec)

(define (hash-ref* d . keys)
  (for/fold ([d d])
            ([k (in-list keys)])
    (hash-ref d k)))

(provide render-tmx-layer)
(define (render-tmx-layer game pos tmx-map-name layer [scale (vec2 1.0)])
  ;(printf "Render tmx: ~a~n" tmx-map-name)
  (let* ([m (hash-ref* game '#:tmx tmx-map-name)]
         [tmx-map (hash-ref m 'map)]
         [guids (hash-ref m 'guids)]
         [sprites (hash-ref game '#:sprites)])
    (for* ([(ts-l? n) (in-indexed (tmx:map-tilesets tmx-map))]
           [ts (if (list? ts-l?) (in-list ts-l?) (in-value ts-l?))])
      ;(printf "Active texture: ~a~nImage: ~a~n" n (tmx:image-source (tmx:tileset-image ts)))
      (active-texture n)
      (let ([tex
              (Atlas-texture
                (Sprite-atlas
                  (hash-ref sprites
                            (tmx:image-source
                              (tmx:tileset-image ts)))))])
        (bind-texture (Texture-binding tex))
      (uniform (string-append "Atlas[" (number->string n) "]") 'i n)
      (uniform (string-append "atlasSize[" (number->string n) "]") 'i (inexact->exact (Texture-width tex)) (inexact->exact (Texture-height tex)))))
    ;(report-gl-error)
    ;(printf "L: ~a~n" (get-uniform-location (string-append "Atlas[0]")))
    (uniform "tileSize" 'i (tmx:map-tile-width tmx-map) (tmx:map-tile-height tmx-map))
    ;(report-gl-error)
    ;(displayln "layerwh")
    (apply uniform "layerWH" 'i (hash-ref layer 'size))
    ;(report-gl-error)
    ;(displayln "viewport")
    (apply uniform "scale" 'f (map exact->inexact (vec2 scale)))
    (uniform "position" 'f (exact->inexact (vec-x pos)) (exact->inexact (vec-y pos)))
    (uniform "rect" 'f 0.0 0.0 (exact->inexact (* (vec-x (hash-ref layer 'size)) (tmx:map-tile-width tmx-map)))
                               (exact->inexact (* (vec-y (hash-ref layer 'size)) (tmx:map-tile-height tmx-map))))
    (apply uniform "viewSize" 'f (map exact->inexact (get-client-size)))
    ;(report-gl-error)
    ;(displayln "tiles")
    (active-texture 9)
    (bind-texture (hash-ref layer 'tile-buffer) 'GL_TEXTURE_BUFFER)
    (uniform "tiles" 'i 9)
    (report-gl-error)
    (enable 'GL_BLEND)
    (blend-function 'GL_SRC_ALPHA 'GL_ONE_MINUS_SRC_ALPHA)
    (draw-elements 'GL_TRIANGLES 6 'GL_UNSIGNED_INT)
    (report-gl-error)))

(provide render-tmx-map)
(define (render-tmx-map game tmx-map-name [pos (vec2 0)] [scale 1.0])
  (let* ([m (hash-ref* game '#:tmx tmx-map-name 'map)])
    (for ([layer (in-list (tmx:map-layers m))])
      (unless (and (not (undefined? (tmx:layer-visible layer)))
                   (= 0 (tmx:layer-visible layer)))
        (render-tmx-layer
          game
          pos
          tmx-map-name
          (hash-ref* game '#:tmx-layers tmx-map-name (tmx:layer-name layer))
          scale)))))
