#version 460 core
in vec2 UV; // Screen coordinates [0 - 1]
out vec4 diffuseColor;
uniform vec4 rect;
uniform sampler2D Atlas[8];
uniform isamplerBuffer tiles;
uniform ivec2 layerWH; // size in TILES
uniform ivec2 tileSize;
uniform ivec2 atlasSize[8]; // in pixels
uniform int time;
void main() {
  vec2 fUV = vec2(UV.x, UV.y);
  vec2 mapTileXY = vec2(floor(fUV.x * rect.z / tileSize.x), floor(fUV.y * rect.w / tileSize.y));
  int mapTileN = int(mapTileXY.x) + int(mapTileXY.y) * int(layerWH.x);
  ivec4 tileData = texelFetch(tiles, mapTileN);
  if (tileData.g == 0) {discard;}
  int tile  = int(tileData.r);
  int atlas = int(tileData.g - 1);
  int flags = int(tileData.b);
  int w = int(atlasSize[atlas].x / tileSize.x);
  ivec2 aUV = ivec2(mod(tile, w), tile / w) * tileSize + ivec2(mod((fUV * rect.zw), tileSize));
  diffuseColor = texelFetch(Atlas[atlas], aUV, 0);
}
