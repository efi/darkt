#version 460 core
in vec3 p;
uniform vec2 viewSize;
uniform vec4 rect;
uniform vec2 position = vec2(0.0, 0.0);
uniform vec2 origin = vec2(0.0, 0.0);
uniform float rotation = 0.0;
uniform vec2 scale = vec2(1.0, 1.0);
out vec2 UV;
const vec4 U = vec4(0, 0, 1, 1); // UVs precomputed for our particular quad
const vec4 V = vec4(1, 0, 1, 0);

void main() {
  // coordinates go from -1 to 1, so 0 to 1 is half the screen
  vec2 vs = 2.0 / viewSize;
  float rot = radians(rotation);
  gl_Position =
    // Initial rectangle
    ((vec4(p, 1.0)

        // Scale to pixel size
        * vec4(rect.z * scale.x, rect.w * scale.y, 1.0, 1.0)

        // Place at origin
        - vec4(origin * scale, 0.0, 0.0))

      // ROTATE
      * mat4(cos(rot), -sin(rot), 0.0, 0.0,
             sin(rot),  cos(rot), 0.0, 0.0,
                  0.0,       0.0, 1.0, 0.0,
                  0.0,       0.0, 0.0, 1.0)

      // Move to final location
      + vec4(position * scale, 0.0, 0.0))

    // Screen space to view space transform
    * mat4(vs.x,  0.0, 0.0, -1.0,
            0.0, vs.y, 0.0, -1.0,
            0.0,  0.0, 1.0,  0.0,
            0.0,  0.0, 0.0,  1.0);
  // the size of a sprite is how many pixels out the viewport it takes, by the post-scale factor
  // align scaled pixels to other scaled pixels so we don't get "half" a pixel increments after scaling up

  int m = int(mod(gl_VertexID, 4)); // UVs for quad, probably unnecessary generalization?
  UV = vec2(U[m], V[m]);
}
