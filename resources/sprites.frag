#version 460 core
uniform sampler2D inTexture;
in vec2 UV;
out vec4 diffuseColor;
uniform vec4 rect;
uniform int time;
void main() {
  vec2 ts = textureSize(inTexture, 0); // size of the texture is 64x64
  vec2 normUV = rect.xy / ts + (UV * rect.zw / ts); // grab only the part that has our sprite
  diffuseColor = texture(inTexture, normUV); // regular sampling or texel sampling should give the same results because we use GL_NEAREST
  //diffuseColor = texelFetch(inTexture, ivec2(UV * WH), 0);
}
