<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.5" name="dungeon" tilewidth="16" tileheight="16" tilecount="72" columns="9">
 <image source="Dungeon Tileset + Spikes.png" width="144" height="128"/>
 <tile id="8">
  <animation>
   <frame tileid="4" duration="250"/>
   <frame tileid="5" duration="250"/>
   <frame tileid="6" duration="250"/>
   <frame tileid="5" duration="250"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="Walls" type="mixed" tile="12">
   <wangcolor name="inside" color="#ff0000" tile="0" probability="1"/>
   <wangtile tileid="0" wangid="1,1,1,1,1,1,1,1"/>
   <wangtile tileid="9" wangid="1,1,1,0,1,1,1,1"/>
   <wangtile tileid="10" wangid="1,1,1,0,0,0,1,1"/>
   <wangtile tileid="11" wangid="1,1,1,1,1,0,1,1"/>
   <wangtile tileid="12" wangid="1,0,0,0,0,0,1,1"/>
   <wangtile tileid="13" wangid="1,1,1,0,0,0,0,0"/>
   <wangtile tileid="18" wangid="1,0,0,0,1,1,1,1"/>
   <wangtile tileid="20" wangid="1,1,1,1,1,0,0,0"/>
   <wangtile tileid="21" wangid="0,0,0,0,1,1,1,0"/>
   <wangtile tileid="22" wangid="0,0,1,1,1,0,0,0"/>
   <wangtile tileid="27" wangid="1,0,1,1,1,1,1,1"/>
   <wangtile tileid="28" wangid="0,0,1,1,1,1,1,0"/>
   <wangtile tileid="29" wangid="1,1,1,1,1,1,1,0"/>
   <wangtile tileid="30" wangid="0,0,1,0,0,0,1,0"/>
   <wangtile tileid="31" wangid="1,0,0,0,1,0,0,0"/>
   <wangtile tileid="36" wangid="0,0,0,0,0,0,1,0"/>
   <wangtile tileid="37" wangid="0,0,1,0,0,0,0,0"/>
   <wangtile tileid="38" wangid="1,0,0,0,0,0,1,0"/>
   <wangtile tileid="39" wangid="1,0,1,0,0,0,0,0"/>
   <wangtile tileid="40" wangid="1,1,1,0,0,0,1,0"/>
   <wangtile tileid="41" wangid="1,0,1,1,1,0,0,0"/>
   <wangtile tileid="45" wangid="1,0,0,0,0,0,0,0"/>
   <wangtile tileid="46" wangid="0,0,0,0,1,0,0,0"/>
   <wangtile tileid="47" wangid="0,0,0,0,1,0,1,0"/>
   <wangtile tileid="48" wangid="0,0,1,0,1,0,0,0"/>
   <wangtile tileid="49" wangid="1,0,0,0,1,0,1,1"/>
   <wangtile tileid="50" wangid="0,0,1,0,1,1,1,0"/>
   <wangtile tileid="51" wangid="1,0,1,1,1,0,1,1"/>
   <wangtile tileid="54" wangid="1,0,1,0,0,0,1,0"/>
   <wangtile tileid="55" wangid="1,0,1,0,1,0,0,0"/>
   <wangtile tileid="56" wangid="1,0,0,0,1,1,1,0"/>
   <wangtile tileid="57" wangid="1,0,1,0,0,0,1,1"/>
   <wangtile tileid="58" wangid="1,0,1,1,1,0,1,0"/>
   <wangtile tileid="59" wangid="1,0,1,0,1,1,1,0"/>
   <wangtile tileid="60" wangid="1,1,1,0,1,1,1,0"/>
   <wangtile tileid="63" wangid="1,0,0,0,1,0,1,0"/>
   <wangtile tileid="64" wangid="0,0,1,0,1,0,1,0"/>
   <wangtile tileid="65" wangid="0,0,1,1,1,0,1,0"/>
   <wangtile tileid="66" wangid="1,1,1,0,1,0,0,0"/>
   <wangtile tileid="67" wangid="1,1,1,0,1,0,1,0"/>
   <wangtile tileid="68" wangid="1,0,1,0,1,0,1,1"/>
   <wangtile tileid="69" wangid="1,0,1,0,1,0,1,0"/>
  </wangset>
 </wangsets>
</tileset>
