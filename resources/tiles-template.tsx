<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.1" name="tiles-template" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="tiles-template.png" width="128" height="128"/>
 <tile id="5">
  <animation>
   <frame tileid="5" duration="250"/>
   <frame tileid="13" duration="250"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="Unnamed Set" type="corner" tile="-1">
   <wangcolor name="pank" color="#ff0000" tile="9" probability="1"/>
   <wangcolor name="teel" color="#00ff00" tile="19" probability="1"/>
   <wangtile tileid="0" wangid="0,2,0,1,0,2,0,2"/>
   <wangtile tileid="1" wangid="0,2,0,1,0,1,0,2"/>
   <wangtile tileid="2" wangid="0,2,0,2,0,1,0,2"/>
   <wangtile tileid="3" wangid="0,1,0,2,0,1,0,1"/>
   <wangtile tileid="4" wangid="0,1,0,1,0,2,0,1"/>
   <wangtile tileid="5" wangid="0,2,0,1,0,2,0,1"/>
   <wangtile tileid="8" wangid="0,1,0,1,0,2,0,2"/>
   <wangtile tileid="9" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="10" wangid="0,2,0,2,0,1,0,1"/>
   <wangtile tileid="11" wangid="0,2,0,1,0,1,0,1"/>
   <wangtile tileid="12" wangid="0,1,0,1,0,1,0,2"/>
   <wangtile tileid="13" wangid="0,1,0,2,0,1,0,2"/>
   <wangtile tileid="16" wangid="0,1,0,2,0,2,0,2"/>
   <wangtile tileid="17" wangid="0,1,0,2,0,2,0,1"/>
   <wangtile tileid="18" wangid="0,2,0,2,0,2,0,1"/>
   <wangtile tileid="19" wangid="0,2,0,2,0,2,0,2"/>
  </wangset>
 </wangsets>
</tileset>
