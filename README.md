# María Engine

This project is a game made with the objective of producing a solid library for game development eventually.

## Linux and Windows
### Dependencies

You will need [Racket](https://racket-lang.org) in order to run or build these sources.
You will also need to install dependencies with the following command.

```
raco pkg install base64 kw-utils sxml vec xenomorph zstd
```

### Building

If you want a compiled version, you run these commands.

```
raco exe main.rkt
```

If something is wrong with the glad library, the `gladloader` folder has a small bash script to build both linux and windows libraries.

## Mac OSX

For mac you will need to first build the glad library inside the gladloader folder to a file named `libglad.dylib`

Then run
```
raco exe main.rkt
```

You may also need to fix up your zstd library like so
```
sudo ln -s /opt/homebrew/Cellar/zstd/1.5.2/lib/libzstd.1.5.2.dylib /usr/local/lib/libzstd.1.dylib
```

Mac is not considered supported and won't be for the near future. Sorry.
